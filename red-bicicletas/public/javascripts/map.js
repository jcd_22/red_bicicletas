var map = L.map('mapid').setView([14.553272, -90.518513], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var marker = L.marker([14.553272, -90.518513]).addTo(map);


var circle = L.circle([14.553272, -90.518559], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(map);

$.ajax({
    dataType:"json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})

marker.bindPopup("<b>Ubicacion actual!</b><br>Casa.").openPopup();

//-----------------agregar bicicletas--------------------------------
